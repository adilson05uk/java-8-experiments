package com.thinkincode.java;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();

        main.runThreadingExample();
        main.runLoopingExample();
        main.runMappingExample();
        main.runInfiniteStreamExample(1, 3);
    }

    private void runThreadingExample() {
        Thread thread1 = new Thread() {

            @Override
            public void run() {
                System.out.println("In thread 1.");
            }
        };

        Thread thread2 = new Thread(() -> System.out.println("In thread 2."));

        thread1.start();
        thread2.start();

        System.out.println("In main thread.");
    }

    private void runLoopingExample() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        numbers.stream()
                .map(String::valueOf) // instance method
                .map(String::toString) // static method
                .forEach(System.out::println);
    }

    private void runMappingExample() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        Collector<Integer, ?, Map<String, Integer>> mapCollector = Collectors.toMap(
                item -> "Item" + item.toString(),
                item -> item
        );

        Map<String, Integer> map = numbers.stream()
                .collect(mapCollector);

        System.out.println(map);
    }

    private void runInfiniteStreamExample(int k, int n) {
        int sum = Stream.iterate(k, e -> e + 1)
                .mapToInt(e -> e)
                .limit(n)
                .sum();

        System.out.println(sum);
    }
}
