package com.thinkincode.java;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class NumbersFilterer {

    @NotNull
    public List<Integer> findAllEvenNumbers(@NotNull List<Integer> numbers) {
        return numbers.stream()
                .filter(this::isEven)
                .collect(Collectors.toList());
    }

    @NotNull
    public Optional<Integer> findFirstEvenNumber(@NotNull List<Integer> numbers) {
        return numbers.stream()
                .filter(this::isEven)
                .findFirst();
    }

    private boolean isEven(int number) {
        return number % 2 == 0;
    }
}
