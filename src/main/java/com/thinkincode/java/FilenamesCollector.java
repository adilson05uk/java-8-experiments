package com.thinkincode.java;

import java.io.File;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class FilenamesCollector {

    public String collect(List<File> files) {
        return files.stream()
                .map(File::getName)
                .collect(joining(", "));
    }
}
