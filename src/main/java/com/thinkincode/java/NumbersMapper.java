package com.thinkincode.java;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

public class NumbersMapper {

    @NotNull
    public List<Integer> doubleAll(@NotNull List<Integer> numbers) {
        return numbers.stream()
                .map(e -> e * 2)
                .collect(Collectors.toList());
    }
}
