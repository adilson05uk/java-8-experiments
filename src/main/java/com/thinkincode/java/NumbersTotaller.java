package com.thinkincode.java;

import java.util.List;

public class NumbersTotaller {

    public int calculateTotal(List<Integer> input) {
        return input.parallelStream()
                .reduce(0, Math::addExact);
    }
}
