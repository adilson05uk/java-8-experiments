package com.thinkincode.java;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class NumbersMapperTest {

    @Test
    public void test_doubleAll_when_list_is_empty() {
        // Given.
        List<Integer> input = Collections.emptyList();

        // When.
        List<Integer> output = new NumbersMapper().doubleAll(input);

        // Then.
        assertEquals(Collections.emptyList(), output);
    }

    @Test
    public void test_doubleAll_when_list_is_non_empty() {
        // Given.
        List<Integer> input = Arrays.asList(0, 1, 2, 3, 4, 5, 6);

        // When.
        List<Integer> output = new NumbersMapper().doubleAll(input);

        // Then.
        assertEquals(Arrays.asList(0, 2, 4, 6, 8, 10, 12), output);
    }
}
