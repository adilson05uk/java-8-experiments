package com.thinkincode.java;

import com.thinkincode.java.NumbersTotaller;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class NumbersTotallerTest {

    @Test
    public void test_calculateTotal_when_list_is_empty() {
        // Given.
        List<Integer> input = Collections.emptyList();

        // When.
        Integer output = new NumbersTotaller().calculateTotal(input);

        // Then.
        assertEquals(new Integer(0), output);
    }

    @Test
    public void test_calculateTotal_when_list_is_non_empty() {
        // Given.
        List<Integer> input = Arrays.asList(1, 2, 3);

        // When.
        Integer output = new NumbersTotaller().calculateTotal(input);

        // Then.
        assertEquals(new Integer(6), output);
    }
}
