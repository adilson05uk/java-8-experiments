package com.thinkincode.java;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class PersonSorterTest {

    @Test
    public void test_sortPersons() {
        // Given.
        PersonSorter.Person alice11 = new PersonSorter.Person("Alice", 11);
        PersonSorter.Person alice12 = new PersonSorter.Person("Alice", 12);
        PersonSorter.Person bob9 = new PersonSorter.Person("Bob", 9);

        List<PersonSorter.Person> input = Arrays.asList(alice12, bob9, alice11);

        // When.
        List<PersonSorter.Person> output = new PersonSorter().sortPersons(input);

        // Then.
        assertEquals(Arrays.asList(alice11, alice12, bob9), output);
    }
}