package com.thinkincode.java;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class PrimeNumberCheckerTest {

    private PrimeNumberChecker target;

    @Before
    public void setUp() {
        target = new PrimeNumberChecker();
    }

    @Test
    public void test_isPrime_when_input_is_1() {
        assertEquals(false, target.isPrime(1));
    }

    @Test
    public void test_isPrime_when_input_is_2() {
        assertEquals(true, target.isPrime(2));
    }

    @Test
    public void test_isPrime_when_input_is_3() {
        assertEquals(true, target.isPrime(3));
    }

    @Test
    public void test_isPrime_when_input_is_4() {
        assertEquals(false, target.isPrime(4));
    }

    @Test
    public void test_isPrime_when_input_is_5() {
        assertEquals(true, target.isPrime(5));
    }

    @Test
    public void test_isPrime_when_input_is_6() {
        assertEquals(false, target.isPrime(6));
    }
}
