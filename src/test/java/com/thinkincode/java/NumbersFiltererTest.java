package com.thinkincode.java;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class NumbersFiltererTest {

    @Test
    public void test_findAllEvenNumbers_when_list_contains_mix_of_odd_and_even_numbers() {
        // Given.
        List<Integer> input = Arrays.asList(0, 1, 2, 3, 4, 5, 6);

        // When.
        List<Integer> output = new NumbersFilterer().findAllEvenNumbers(input);

        // Then.
        assertEquals(Arrays.asList(0, 2, 4, 6), output);
    }

    @Test
    public void test_findFirstEvenNumber_when_list_contains_zero_only() {
        // Given.
        List<Integer> input = Collections.singletonList(0);

        // When.
        Optional<Integer> output = new NumbersFilterer().findFirstEvenNumber(input);

        // Then.
        assertEquals(Optional.of(0), output);
    }

    @Test
    public void test_findFirstEvenNumber_when_list_contains_mix_of_odd_and_even_numbers() {
        // Given.
        List<Integer> input = Arrays.asList(1, 2, 3, 4, 5, 6);

        // When.
        Optional<Integer> output = new NumbersFilterer().findFirstEvenNumber(input);

        // Then.
        assertEquals(Optional.of(2), output);
    }

    @Test
    public void test_findFirstEvenNumber_when_list_contains_odd_numbers_only() {
        // Given.
        List<Integer> input = Arrays.asList(1, 3, 5);

        // When.
        Optional<Integer> output = new NumbersFilterer().findFirstEvenNumber(input);

        // Then.
        assertEquals(Optional.empty(), output);
    }
}
