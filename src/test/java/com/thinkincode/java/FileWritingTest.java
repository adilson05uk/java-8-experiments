package com.thinkincode.java;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class FileWritingTest {

    @Test
    public void test_file_writing() throws Exception {
        File folder = new File("build/test-output");

        if (!folder.exists()) {
            assertTrue(folder.mkdirs());
        }

        File file = new File(folder, "foo.txt");

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        FileInputStream fileInputStream = new FileInputStream(file);

        fileOutputStream.write('a');
        char value1 = (char) fileInputStream.read();

        fileOutputStream.write('b');
        char value2 = (char) fileInputStream.read();

        assertEquals('a', value1);
        assertEquals('b', value2);

        assertEquals(-1, fileInputStream.read());
    }
}
