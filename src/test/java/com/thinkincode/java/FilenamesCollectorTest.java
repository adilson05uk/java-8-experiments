package com.thinkincode.java;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class FilenamesCollectorTest {

    @Test
    public void test_collect() {
        List<File> files = Arrays.asList(
                new File("SomeDir/SomeFileA"),
                new File("SomeDir/SomeFileB"),
                new File("SomeDir/SomeFileC")
        );

        // When.
        String output = new FilenamesCollector().collect(files);

        // Then.
        assertEquals("SomeFileA, SomeFileB, SomeFileC", output);
    }
}
